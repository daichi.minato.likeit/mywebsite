package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Beans.user;

public class DAO {

	public user findByLogin(String UserloginId, String Userpassword) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE user_loginId = ? and user_password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, UserloginId);
			pStmt.setString(2, Userpassword);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String userloginIdData = rs.getString("user_loginId");
			String usernameData = rs.getString("user_name");
			return new user(userloginIdData, usernameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public  int findBySingUp(String UserloginId, String Username,String Userpassword) {
		Connection conn = null;
		int rs = 0;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "INSERT INTO user (user_loginId, user_name,user_password,user_create_date,user_update_date)VALUES(?,?,?,now(),now())";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, UserloginId);
			pStmt.setString(2, Username);
			pStmt.setString(3, Userpassword);

			rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return (Integer) null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return (Integer) null;
				}
			}
		}
		return rs;
	}
}
