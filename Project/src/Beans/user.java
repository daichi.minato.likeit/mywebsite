package Beans;

import java.io.Serializable;

public class user implements Serializable {

	private int id;
	private String UserloginId;
	private String Username;
	private String Userpassword;
	private String UsercreateDate;
	private String UserupdateDate;

	public user(String UserloginId, String Username) {
		this.UserloginId = UserloginId;
		this.Username = Username;
	}

	public user(int id, String UserloginId, String Username, String Userpassword, String UsercreateDate,
			String UserupdateDate) {
		this.id = id;
		this.UserloginId = UserloginId;
		this.Username = Username;
		this.Userpassword = Userpassword;
		this.UsercreateDate = UsercreateDate;
		this.UserupdateDate = UserupdateDate;
	}

	public user(String UserloginId, String Username, String Userpassword) {
		this.UserloginId = UserloginId;
		this.Username = Username;
		this.Userpassword = Userpassword;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserloginId() {
		return UserloginId;
	}

	public void setUserloginId(String userloginId) {
		UserloginId = userloginId;
	}

	public String getUsername() {
		return Username;
	}

	public void setUsername(String username) {
		Username = username;
	}

	public String getUserpassword() {
		return Userpassword;
	}

	public void setUserpassword(String userpassword) {
		Userpassword = userpassword;
	}

	public String getUsercreateDate() {
		return UsercreateDate;
	}

	public void setUsercreateDate(String usercreateDate) {
		UsercreateDate = usercreateDate;
	}

	public String getUserupdateDate() {
		return UserupdateDate;
	}

	public void setUserupdateDate(String userupdateDate) {
		UserupdateDate = userupdateDate;
	}

}
