package Servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Beans.user;

/**
 * Servlet implementation class SingUp
 */
@WebServlet("/SingUp")
public class SingUp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SingUp() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sign_up.jsp");
		dispatcher.forward(request, response);
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String login_id = request.getParameter("UserloginId");
		String name = request.getParameter("Username");
		String password = request.getParameter("Userpassword");
		String password2 = request.getParameter("Userpassword2");

		if (!password.equals(password2)) {
			request.setAttribute("errMsg", "パスワードが一致しません。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sign_up.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if ((login_id.equals("")) || (name.equals("")) || (password.equals("")) || (password2.equals(""))) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sign_up.jsp");
			dispatcher.forward(request, response);
			return;
		}

		user user = new user(login_id,name,password);

		request.setAttribute("user", user);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Registration_confirmation.jsp");
		dispatcher.forward(request, response);
	}
}
