package Servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.DAO;

/**
 * Servlet implementation class registrationConfirmation
 */
@WebServlet("/registrationConfirmation")
public class registrationConfirmation extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public registrationConfirmation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		String login_id = request.getParameter("UserloginId");
		String name = request.getParameter("Username");
		String password = request.getParameter("Userpassword");
		String password2 = request.getParameter("Userpassword2");
		DAO dao = new DAO();
		int user= dao.findBySingUp(login_id, name,password);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Completion_of_registration.jsp");
		dispatcher.forward(request, response);
		return;
	}

	}


