<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
      <link rel="stylesheet" href="./css/css.css" type="text/css">
</head>
	<body>
         <div style="background-color: #000;">
		<h1 style="color:white" align="center">🎙HIPHOP Academy🎙</h1>
               <h2 style="color:white" align="center">ログイン</h2>
        <div style="text-align:center;">
             <img src="./img/cyansu.jpg" width="300">
             </div>
             <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	<form class="form-signin" action="Login" method="post">
        	<div class="form-group" align="center">
                <label for="exampleFormControlSelect1" style="color:white">ログインID</label>
             </div>
                <div class="form-group" align="center">
                <input type="text" name="UserloginId"size="30">
        </div>
        	<div class="form-group" align="center">
        <label for="exampleFormControlSelect1" style="color:white">パスワード</label>
             </div>
                <div class="form-group" align="center">
                <input type="password" name="Userpassword"size="30">
        </div>
        	<div class="form-group" align="center">
<button type="submit" class="btn btn--white">GOGO</button>
        </div>
        </form>
        <br>
        <div class="form-group" align="center">
        <a href="sign_up" class="btn btn--white">新規登録</a>
        </div>
        </div>
	</body>
</html>
