<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet" href="./css/css.css" type="text/css">
</head>
<body>
	<div style="background-color: #000;">
		<h1 style="color: white" align="center">🎙HIPHOP Academy🎙</h1>
		<h2 style="color: white" align="center">登録確認</h2>
		<div style="text-align: center;">
			<img src="./img/migo.jpg" width="300">
		</div>
		<form method="post" action="registrationConfirmation">
			<div class="form-group" align="center">
				<label for="exampleFormControlSelect1" style="color: white">ログインID</label>
			</div>
			<div class="form-group" align="center">${user.UserloginId }
			<input type="hidden" name="UserloginId" value="${user.UserloginId }">
			</div>
			<div class="form-group" align="center">
				<label for="exampleFormControlSelect1" style="color: white">パスワード</label>
			</div>
			<div class="form-group" align="center">${user.Userpassword }
			<input type="hidden" name="Userpassword"value="${user.Userpassword }">
			</div>
			<div class="form-group" align="center">
				<label for="exampleFormControlSelect1" style="color: white">ユーザー名</label>
			</div>

			<div class="form-group" align="center">${user.Username }
			<input type="hidden" name="Userpassword2" value="${user.Username }">
			</div>
			<div class="form-group" align="center">
				<label for="exampleFormControlSelect1" style="color: white">こちらでよろしいですか？</label>
			</div>
			<div class="form-group" align="center"></div>
			<div class="form-group" align="center">
				<button type="submit" class="btn btn--white">YES</button>
				<a href="sing_up"class="btn btn--white">NO</a>
			</div>
		</form>
	</div>
</body>
</html>
